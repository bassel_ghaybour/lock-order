package com.beeorder.orderslockmc.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "operation_user")
@Setter
@Getter
public class UserEntity implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @JsonProperty(value = "role_id")
    @Column(name = "role_id")
    private Integer roleId;
}
