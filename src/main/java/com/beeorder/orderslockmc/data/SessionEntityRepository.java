package com.beeorder.orderslockmc.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

import javax.transaction.Transactional;

public interface SessionEntityRepository extends CrudRepository<SessionEntity, String> {


    SessionEntity getSessionEntityById(@NotNull String sessionId);

    SessionEntity getSessionEntityByIdAndReleasedTimeIsNull(@NotNull String sessionId);

    @Async
    @Transactional
    @Modifying
    @Query(value = "update operation_user_sessions set release_time = current_timestamp where id = :sessionId", nativeQuery = true)
    void releaseSession(@NotNull String sessionId);

    @Async
    @Transactional
    @Modifying
    @Query(value = "update operation_user_sessions set release_time = current_timestamp where release_time is null", nativeQuery = true)
    void releaseAllSessions();

    @Async
    @Transactional
    @Modifying
    @Query(value = "update operation_user_sessions set release_time = current_timestamp where agent_id = :userId", nativeQuery = true)
    void releaseSessionByUserId(@NotNull Long userId);


}
