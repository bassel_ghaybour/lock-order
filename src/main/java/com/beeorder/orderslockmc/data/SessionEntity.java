package com.beeorder.orderslockmc.data;

import com.beeorder.orderslockmc.models.SessionDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "operation_user_sessions")
@Setter
@Getter
public class SessionEntity implements Serializable {

    @JsonProperty("id")
    @Id
    @Column(name = "id")
    private String id;

    @JsonIgnore
    @NotNull
    @Column(name = "agent_id")
    private Long agentId;

    @JsonIgnore
    @NotNull
    @Column(name = "create_time")
    private final Date createTime = Calendar.getInstance().getTime();

    @JsonIgnore
    @Nullable
    @Column(name = "release_time")
    private Date releasedTime = null;


    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", insertable = false, updatable = false)
    private UserEntity agent = null;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", insertable = false, updatable = false)
    private RestaurantUserEntity restaurantUserEntity = null;


    public SessionDomain toDomain() {
        SessionDomain sessionDomain = new SessionDomain();
        sessionDomain.setId(id);

        if (null == restaurantUserEntity)
            sessionDomain.setAgent(agent);
        else
            sessionDomain.setAgent(restaurantUserEntity);

        return sessionDomain;
    }
}
