package com.beeorder.orderslockmc.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;

import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface OrderStateEntityRepository extends CrudRepository<OrderStateEntity, Long> {

    @Async
    CompletableFuture<OrderStateEntity>
    findFirstByBillIdAndInProgressTrueOrderByUpdateTimestampDesc(@NotNull @Param("billId") Long billId);

    @Async
    CompletableFuture<OrderStateEntity>
    findFirstBySessionIdAndBillIdAndInProgressTrueOrderByUpdateTimestampDesc(@NotNull String sessionId, @NotNull Long billId);

    @Async
    CompletableFuture<List<OrderStateEntity>>
    getOrderStateEntitiesBySessionIdAndActionNameAndInProgressTrue(@NotNull String sessionId, @NotNull ActionType actionType);

    @Transactional
    @Modifying
    @Query(value = "update OrderStateEntity set inProgress = false where actionName = 'LOCKED' and inProgress = true")
    int releaseAllOrdersForAllSessions();

    @Async
    @Query(value = "select queue.* from orders_queue_logs as queue where queue.in_progress = 1 and queue.action_taken ='LOCKED';", nativeQuery = true)
    CompletableFuture<List<OrderStateEntity>> getAllOrdersStatus();
}
