package com.beeorder.orderslockmc.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {

    UserEntity getUserEntitiesById(Long id);

    @Async
    @Query(value = "select user.* from user where user.id = :id", nativeQuery = true)
    CompletableFuture<UserEntity> asyncGetUserById(Long id);

    @Async
    @Query(value = "select t.* from operation_user as t where t.email = :email and t.token = :deviceId", nativeQuery = true)
    CompletableFuture<UserEntity> findUserEntityByEmailAndDeviceId(@NotNull String email, @NotNull String deviceId);
}
