package com.beeorder.orderslockmc.data;

public enum ActionType {
    LOCKED, RELEASED
}
