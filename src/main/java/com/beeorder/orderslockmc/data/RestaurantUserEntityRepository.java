package com.beeorder.orderslockmc.data;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface RestaurantUserEntityRepository extends CrudRepository<RestaurantUserEntity, Long> {


    RestaurantUserEntity getRestaurantUserEntityById(@NotNull Long id);

    @Query(value = "select t.* from restaurant as t where t.restaurant_username = :email and t.restaurant_token = :deviceId", nativeQuery = true)
    RestaurantUserEntity findUserEntityByEmailAndDeviceId(@NotNull String email, @NotNull String deviceId);
}
