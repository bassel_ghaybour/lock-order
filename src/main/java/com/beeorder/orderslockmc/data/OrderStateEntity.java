package com.beeorder.orderslockmc.data;

import com.beeorder.orderslockmc.models.OrderStateDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "orders_queue_logs")
@Setter
@Getter
public class OrderStateEntity implements Serializable {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonProperty(value = "bill_id")
    @NotNull
    @Column(name = "bill_id")
    private Long billId;

    @JsonProperty(value = "action_type")
    @NotNull
    @Column(name = "action_taken")
    @Enumerated(EnumType.STRING)
    private ActionType actionName;

    @JsonProperty(value = "update_time")
    @NotNull
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_time")
    private Date updateTimestamp = Calendar.getInstance().getTime();

    @NotNull
    @JsonProperty(value = "in_progress")
    @Column(name = "in_progress")
    private Boolean inProgress;

    @JsonIgnore
    @NotNull
    @Column(name = "session_id")
    private String sessionId;

    @JsonProperty("session")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "session_id", referencedColumnName = "id", insertable = false, updatable = false)
    private SessionEntity session;


    public OrderStateDomain toDomain() {
        OrderStateDomain orderStateDomain = new OrderStateDomain();
        orderStateDomain.setBillId(billId);
        orderStateDomain.setActionName(actionName);
        orderStateDomain.setInProgress(inProgress);
        orderStateDomain.setUpdateTimestamp(updateTimestamp);
        orderStateDomain.setSession(session.toDomain());
        return orderStateDomain;
    }
}
