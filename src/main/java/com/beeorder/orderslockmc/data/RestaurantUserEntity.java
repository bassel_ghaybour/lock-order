package com.beeorder.orderslockmc.data;

import com.beeorder.orderslockmc.models.RestaurantUserDomain;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Table(name = "restaurant")
@Setter
@Getter
public class RestaurantUserEntity implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;


    public RestaurantUserDomain toDomain() {
        RestaurantUserDomain domain = new RestaurantUserDomain();
        domain.setId(id);
        domain.setName(name);

        return domain;
    }
}
