package com.beeorder.orderslockmc.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class SessionDomain {

    @JsonProperty("id")
    private String id;

    @JsonProperty("agent")
    private Serializable agent;

}
