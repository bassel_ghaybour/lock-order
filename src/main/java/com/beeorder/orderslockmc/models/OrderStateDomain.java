package com.beeorder.orderslockmc.models;

import com.beeorder.orderslockmc.data.ActionType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class OrderStateDomain {

    @JsonProperty(value = "bill_id")
    private Long billId;

    @JsonProperty(value = "action_type")
    private ActionType actionName;

    @JsonProperty(value = "update_time")
    private Date updateTimestamp;

    @JsonProperty(value = "in_progress")
    private Boolean inProgress;

    @JsonProperty("session")
    private SessionDomain session;
}
