package com.beeorder.orderslockmc.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Setter
@Getter
public class ResponseWrapper<T> implements Serializable {

    @JsonProperty(value = "code")
    private int code;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "payload")
    private final T payload;

    @JsonProperty(value = "sent_time")
    private final Date sentTime;

    public ResponseWrapper(T payload) {
        sentTime = Calendar.getInstance().getTime();
        this.payload = payload;
    }
}
