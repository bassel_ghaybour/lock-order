package com.beeorder.orderslockmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableEurekaClient
public class OrdersLockMcApplication {

    @Bean(name = "applicationTaskExecutor")
    public Executor getExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(50);
        executor.setMaxPoolSize(500);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("OrdersStateManagementAsync-");
        executor.initialize();
        return executor;
    }

    @Bean
    public WebClient getWebClient() {
        return WebClient.builder().build();
    }

    public static void main(String[] args) {
        SpringApplication.run(OrdersLockMcApplication.class, args);
    }

}
