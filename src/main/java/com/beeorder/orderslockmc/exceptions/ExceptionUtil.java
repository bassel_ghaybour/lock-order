package com.beeorder.orderslockmc.exceptions;

import com.beeorder.orderslockmc.models.ResponseWrapper;
import com.beeorder.orderslockmc.util.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static com.beeorder.orderslockmc.exceptions.TypeOfException.*;

public abstract class ExceptionUtil {

    private static final Map<TypeOfException, String> EXCEPTION_INFO_MESSAGES = new HashMap<>();

    static {
        EXCEPTION_INFO_MESSAGES.put(GENERAL_ERROR, "Oops!, unexpected error has occurred, maybe you provide invalid data or messing to provide some required data.");
        EXCEPTION_INFO_MESSAGES.put(ORDER_NOT_FOUND, "Oops!,this order not found.");
        EXCEPTION_INFO_MESSAGES.put(ORDER_ALREADY_LOCKED, "Oops!, this order locked.");
        EXCEPTION_INFO_MESSAGES.put(ORDER_ALREADY_RELEASED, "Oops!, this order already released by you.");
        EXCEPTION_INFO_MESSAGES.put(USER_NOT_FOUND, "Oops!, you sent invalid user info.");
        EXCEPTION_INFO_MESSAGES.put(ERROR_ORDER_ALREADY_DONE, "Oops!, this order already done.");
        EXCEPTION_INFO_MESSAGES.put(DATABASE_EXCEPTION, "Oops!, this is really shameful but this is a database error we are sorry.");
        EXCEPTION_INFO_MESSAGES.put(HAVE_NOT_ACTIVE_SESSION, "Oops!, you don't have an active session please refresh the web page or login again");
        EXCEPTION_INFO_MESSAGES.put(HAVE_NOT_VALID_SESSION, "Oops!, all orders you have locked are not released so please immediately report this issue to the system administrator");
    }


    public static ResponseEntity<?> getExceptionResponse(TypeOfException typeOfException) {
        ResponseWrapper<?> responseWrapper = new ResponseWrapper<>(null);
        responseWrapper.setCode(TypeOfException.HAVE_NOT_ACTIVE_SESSION == typeOfException ? Constants.UNAUTHORIZED_ERROR_CODE : Constants.FAILURE_CODE);
        responseWrapper.setMessage(EXCEPTION_INFO_MESSAGES.get(typeOfException));

        return new ResponseEntity<>(responseWrapper, HttpStatus.OK);
    }
}
