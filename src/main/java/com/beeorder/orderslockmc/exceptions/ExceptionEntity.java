package com.beeorder.orderslockmc.exceptions;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "socket_unexpected_exceptions")
@Setter
@Getter
public class ExceptionEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "create_date")
    private final Date createTime = Calendar.getInstance().getTime();

    @NotNull
    @Column(name = "exception_message")
    private String exceptionMessage;
}
