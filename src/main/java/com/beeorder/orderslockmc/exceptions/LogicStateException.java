package com.beeorder.orderslockmc.exceptions;

import lombok.Getter;

@Getter
public class LogicStateException extends IllegalStateException {

    private final TypeOfException typeOfException;

    public LogicStateException(TypeOfException typeOfException) {
        this.typeOfException = typeOfException;
    }

}
