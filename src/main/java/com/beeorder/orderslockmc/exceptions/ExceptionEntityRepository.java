package com.beeorder.orderslockmc.exceptions;

import org.springframework.data.repository.CrudRepository;

public interface ExceptionEntityRepository extends CrudRepository<ExceptionEntity, Integer> {
}
