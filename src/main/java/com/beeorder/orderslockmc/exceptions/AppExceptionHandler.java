package com.beeorder.orderslockmc.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    private final ExceptionEntityRepository exceptionEntityRepository;

    public AppExceptionHandler(ExceptionEntityRepository exceptionEntityRepository) {
        this.exceptionEntityRepository = exceptionEntityRepository;
    }

    @ExceptionHandler(value = {LogicStateException.class})
    public ResponseEntity<?> handleExpectedExceptions(LogicStateException e) {
        return ExceptionUtil.getExceptionResponse(e.getTypeOfException());
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<?> handleUnexpectedExceptions(Exception e) {

        try {
            ExceptionEntity exceptionEntity = new ExceptionEntity();
            exceptionEntity.setExceptionMessage(setStackTrace(e));
            exceptionEntityRepository.save(exceptionEntity);
        } catch (Exception ignore) {
            return ExceptionUtil.getExceptionResponse(TypeOfException.DATABASE_EXCEPTION);
        }


        return ExceptionUtil.getExceptionResponse(TypeOfException.GENERAL_ERROR);
    }


    private String setStackTrace(Exception e) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("-------------------------------------------------------------\n")
                .append("Exception message: ").append(e.getMessage()).append("\n")
                .append("-------------------------------------------------------------\n");

        if (null != e.getStackTrace() && 0 < e.getStackTrace().length)
            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                stringBuilder.append("File Name: ").append(stackTraceElement.getFileName()).append("\n")
                        .append("Class Name").append(stackTraceElement.getClassName()).append("\n")
                        .append("Method Name").append(stackTraceElement.getMethodName()).append("\n")
                        .append("Line Number: ").append(stackTraceElement.getLineNumber())
                        .append("\n-------------------------------------------------------------");
            }

        return stringBuilder.toString();
    }
}
