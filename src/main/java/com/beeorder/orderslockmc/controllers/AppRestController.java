package com.beeorder.orderslockmc.controllers;

import com.beeorder.orderslockmc.models.ResponseWrapper;
import com.beeorder.orderslockmc.services.ManageStateService;
import com.beeorder.orderslockmc.services.SyncService;
import com.beeorder.orderslockmc.services.UserInfo;
import com.beeorder.orderslockmc.util.AppUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(path = "/app")
public class AppRestController {

    private final SyncService syncService;
    private final ManageStateService manageStateService;
    private final UserInfo userInfo;

    @Autowired
    public AppRestController(SyncService syncService, ManageStateService manageStateService, UserInfo userInfo) {
        this.syncService = syncService;
        this.manageStateService = manageStateService;
        this.userInfo = userInfo;
    }

    @Async
    @GetMapping(path = "full-sync")
    public CompletableFuture<ResponseWrapper<?>> fullSync() {
        return syncService.getAsyncFullSync().thenApply(AppUtil::getSuccessWrapper);
    }

    @Async
    @PutMapping(path = "lock-order")
    public CompletableFuture<ResponseWrapper<?>> lockOrder(@RequestParam("sessionId") String sessionId, @RequestParam("billId") Long billId) {
        return manageStateService.lockOrderForSession(sessionId, billId).thenApply(AppUtil::getSuccessWrapper);
    }

    @Async
    @PutMapping(path = "unlock-order")
    public CompletableFuture<ResponseWrapper<?>> unlockOrder(@RequestParam("sessionId") String sessionId, @RequestParam("billId") Long billId) {
        return manageStateService.unlockOrderForSession(sessionId, billId).thenApply(AppUtil::getSuccessWrapper);
    }

    @Async
    @PostMapping(path = "release-session")
    public CompletableFuture<ResponseWrapper<?>> releaseOrdersLockedByAgent(@RequestParam("sessionId") String sessionId) {
        return manageStateService.sessionRelease(sessionId).thenApply(AppUtil::getSuccessWrapper);
    }


    @Async
    @PostMapping(path = "set-session")
    public CompletableFuture<ResponseWrapper<?>> setSession(@RequestParam("sessionId") String sessionId, @RequestParam("agentId") Long agentId) {
        return syncService.registerSession(sessionId, agentId).thenApply(AppUtil::getSuccessWrapper);
    }

    @Async
    @PutMapping(path = "release-all")
    public CompletableFuture<ResponseWrapper<?>> resetAllOrders() {
        return manageStateService.releaseAllOrders().thenApply(AppUtil::getSuccessWrapper);
    }


    @Async
    @GetMapping(path = "check-user")
    public CompletableFuture<ResponseWrapper<?>> checkUser(@NotNull @RequestParam("email") String email, @NotNull @RequestParam("token") String token) {
        return userInfo.getUserByEmailAndToken(email, token).thenApply(AppUtil::getSuccessWrapper);
    }
}
