package com.beeorder.orderslockmc.services;

import com.beeorder.orderslockmc.data.ActionType;
import com.beeorder.orderslockmc.data.OrderStateEntity;
import com.beeorder.orderslockmc.exceptions.LogicStateException;
import org.jetbrains.annotations.Nullable;

import static com.beeorder.orderslockmc.exceptions.TypeOfException.*;

public abstract class Validator {

    public static void orderShouldNotLocked(@Nullable OrderStateEntity orderStateEntity) {

        if (null == orderStateEntity)
            return;

        if (ActionType.LOCKED == orderStateEntity.getActionName())
            throw new LogicStateException(ORDER_ALREADY_LOCKED);
    }

    public static void orderShouldNotReleased(@Nullable OrderStateEntity orderStateEntity) {
        if (null == orderStateEntity || ActionType.RELEASED == orderStateEntity.getActionName())
            throw new LogicStateException(ORDER_ALREADY_RELEASED);
    }
}
