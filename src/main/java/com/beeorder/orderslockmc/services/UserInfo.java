package com.beeorder.orderslockmc.services;

import com.beeorder.orderslockmc.data.RestaurantUserEntity;
import com.beeorder.orderslockmc.data.RestaurantUserEntityRepository;
import com.beeorder.orderslockmc.data.UserEntity;
import com.beeorder.orderslockmc.data.UserEntityRepository;
import com.beeorder.orderslockmc.exceptions.LogicStateException;
import com.beeorder.orderslockmc.exceptions.TypeOfException;
import org.jetbrains.annotations.NotNull;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.concurrent.CompletableFuture;

@Service
public class UserInfo {

    private final UserEntityRepository userEntityRepository;
    private final RestaurantUserEntityRepository restaurantUserEntityRepository;

    public UserInfo(UserEntityRepository userEntityRepository, RestaurantUserEntityRepository restaurantUserEntityRepository) {
        this.userEntityRepository = userEntityRepository;
        this.restaurantUserEntityRepository = restaurantUserEntityRepository;
    }


    public UserEntity getUserInfo(@NotNull Long agentId) {
        return userEntityRepository.getUserEntitiesById(agentId);
    }

    public RestaurantUserEntity getRestaurantUserInfo(@NotNull Long restaurantAgentId) {
        return restaurantUserEntityRepository.getRestaurantUserEntityById(restaurantAgentId);
    }

    @Deprecated
    public CompletableFuture<UserEntity> getUserInfoAsync(@NotNull Long agentId) {
        return userEntityRepository.asyncGetUserById(agentId).thenApply(userEntity -> {
            userIsFound(userEntity);
            return userEntity;
        });
    }

    @Async
    public CompletableFuture<Serializable> getUserByEmailAndToken(@NotNull String email, @NotNull String token) {
        return userEntityRepository.findUserEntityByEmailAndDeviceId(email, token)
                .thenApply(userEntity -> {
                    if (null == userEntity) {
                        RestaurantUserEntity restaurantUserEntity = restaurantUserEntityRepository.findUserEntityByEmailAndDeviceId(email, token);
                        userIsFound(restaurantUserEntity);
                        return restaurantUserEntity.toDomain();
                    }

                    return userEntity;
                });
    }


    private void userIsFound(Serializable userEntity) {
        if (null == userEntity)
            throw new LogicStateException(TypeOfException.USER_NOT_FOUND);
    }
}
