package com.beeorder.orderslockmc.services;

import com.beeorder.orderslockmc.data.*;
import com.beeorder.orderslockmc.exceptions.LogicStateException;
import com.beeorder.orderslockmc.exceptions.TypeOfException;
import com.beeorder.orderslockmc.models.OrderStateDomain;
import com.beeorder.orderslockmc.models.SessionDomain;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class SyncService {

    private final OrderStateEntityRepository orderStateEntityRepository;
    private final UserInfo userInfo;
    private final SessionEntityRepository sessionEntityRepository;

    @Autowired
    public SyncService(OrderStateEntityRepository orderStateEntityRepository, UserInfo userInfo, SessionEntityRepository sessionEntityRepository) {
        this.orderStateEntityRepository = orderStateEntityRepository;
        this.userInfo = userInfo;
        this.sessionEntityRepository = sessionEntityRepository;
    }

    @Async
    public CompletableFuture<List<OrderStateDomain>> getAsyncFullSync() {
        return orderStateEntityRepository.getAllOrdersStatus()
                .thenApply(orderStateEntities -> orderStateEntities.stream().map(OrderStateEntity::toDomain).collect(Collectors.toList()));
    }

    @Async
    public CompletableFuture<SessionDomain> registerSession(@NotNull String sessionId, @NotNull Long agentId) {

        UserEntity agent = userInfo.getUserInfo(agentId);
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setId(sessionId);
        sessionEntity.setAgentId(agentId);

        if (null != agent)
            sessionEntity.setAgent(agent);
        else {
            RestaurantUserEntity restaurantUserEntity = userInfo.getRestaurantUserInfo(agentId);

            if (null == restaurantUserEntity)
                throw new LogicStateException(TypeOfException.USER_NOT_FOUND);

            sessionEntity.setRestaurantUserEntity(restaurantUserEntity);
        }
        sessionEntityRepository.releaseSessionByUserId(agentId);
        SessionEntity resultSession = sessionEntityRepository.save(sessionEntity);

        return CompletableFuture.completedFuture(resultSession.toDomain());

    }
}
