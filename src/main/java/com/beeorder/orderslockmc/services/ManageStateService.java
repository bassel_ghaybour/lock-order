package com.beeorder.orderslockmc.services;


import com.beeorder.orderslockmc.data.*;
import com.beeorder.orderslockmc.exceptions.LogicStateException;
import com.beeorder.orderslockmc.exceptions.TypeOfException;
import com.beeorder.orderslockmc.models.OrderStateDomain;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import static com.beeorder.orderslockmc.services.Validator.*;

@Service
public class ManageStateService {

    private final OrderStateEntityRepository orderStateEntityRepository;
    private final SessionEntityRepository sessionEntityRepository;
    private final Executor executor;

    public ManageStateService(OrderStateEntityRepository orderStateEntityRepository,
                              SessionEntityRepository sessionEntityRepository, @Qualifier("applicationTaskExecutor") Executor executor) {
        this.orderStateEntityRepository = orderStateEntityRepository;
        this.sessionEntityRepository = sessionEntityRepository;
        this.executor = executor;
    }

    @Async
    public CompletableFuture<Integer> releaseAllOrders() {
        sessionEntityRepository.releaseAllSessions();
        return CompletableFuture.completedFuture(orderStateEntityRepository.releaseAllOrdersForAllSessions());
    }

    @Async
    public CompletableFuture<List<OrderStateDomain>> lockOrderForSession(@NotNull String sessionId, @NotNull Long billId) {
        return orderStateEntityRepository.findFirstByBillIdAndInProgressTrueOrderByUpdateTimestampDesc(billId)
                .thenApply(orderStateEntity -> {
                    orderShouldNotLocked(orderStateEntity);
                    updateOrderState(orderStateEntity);
                    return Collections.singletonList(orderStateEntityRepository.save(createLockedOrder(hasActiveSession(sessionId), billId, ActionType.LOCKED)).toDomain());
                });
    }

    @Async
    public CompletableFuture<List<OrderStateDomain>> unlockOrderForSession(@NotNull String sessionId, @NotNull Long billId) {
        return orderStateEntityRepository.findFirstBySessionIdAndBillIdAndInProgressTrueOrderByUpdateTimestampDesc(sessionId, billId)
                .thenApply(orderStateEntity -> {
                    orderShouldNotReleased(orderStateEntity);
                    updateOrderState(orderStateEntity);
                    return Collections.singletonList(orderStateEntityRepository.save(createLockedOrder(hasActiveSession(sessionId), billId, ActionType.RELEASED)).toDomain());
                });
    }

    @Async
    public CompletableFuture<List<OrderStateDomain>> sessionRelease(@NotNull String sessionId) {
        SessionEntity sessionEntity = hasValidSession(sessionId);
        sessionEntityRepository.releaseSession(sessionId);
        return orderStateEntityRepository.getOrderStateEntitiesBySessionIdAndActionNameAndInProgressTrue(sessionId, ActionType.LOCKED)
                .thenApply(orderStateEntities -> {
                    updateAll(orderStateEntities);
                    return ((List<OrderStateEntity>) orderStateEntityRepository.saveAll(orderStateEntities.stream().map(orderStateEntity -> createLockedOrder(sessionEntity, orderStateEntity.getBillId(), ActionType.RELEASED)).collect(Collectors.toList())))
                            .stream().map(OrderStateEntity::toDomain).collect(Collectors.toList());
                });
    }

    private OrderStateEntity createLockedOrder(@NotNull SessionEntity sessionEntity, @Nullable Long billId, @NotNull ActionType actionType) {
        OrderStateEntity newOrderState = new OrderStateEntity();
        newOrderState.setSessionId(sessionEntity.getId());
        newOrderState.setActionName(actionType);
        newOrderState.setBillId(billId);
        newOrderState.setInProgress(ActionType.RELEASED != actionType);
        newOrderState.setSession(sessionEntity);

        return newOrderState;
    }

    private SessionEntity hasActiveSession(@NotNull String sessionId) {
        // TODO: 9/15/2020 this is a bug when connected user has another session
        SessionEntity sessionEntity = sessionEntityRepository.getSessionEntityByIdAndReleasedTimeIsNull(sessionId);

        if (null == sessionEntity) {
            throw new LogicStateException(TypeOfException.HAVE_NOT_ACTIVE_SESSION);
        }

        return sessionEntity;
    }

    private SessionEntity hasValidSession(@NotNull String sessionId) {
        SessionEntity sessionEntity = sessionEntityRepository.getSessionEntityById(sessionId);

        if (null == sessionEntity) {
            throw new LogicStateException(TypeOfException.HAVE_NOT_VALID_SESSION);
        }

        return sessionEntity;
    }

    private void updateOrderState(@Nullable OrderStateEntity orderStateEntity) {
        if (null != orderStateEntity) {
            executor.execute(() -> {
                orderStateEntity.setInProgress(false);
                orderStateEntityRepository.save(orderStateEntity);
            });
        }
    }

    private void updateAll(@Nullable Iterable<OrderStateEntity> iterable) {
        if (null != iterable) {
            executor.execute(() -> {
                for (OrderStateEntity entity :
                        iterable) {
                    entity.setInProgress(false);
                }
                orderStateEntityRepository.saveAll(iterable);
            });
        }
    }
}
