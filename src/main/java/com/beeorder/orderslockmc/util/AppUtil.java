package com.beeorder.orderslockmc.util;

import com.beeorder.orderslockmc.models.ResponseWrapper;

public abstract class AppUtil {

    public static final String SUCCESS_MESSAGE = "Success";


    public static <T> ResponseWrapper<?> getSuccessWrapper(T payload) {
        ResponseWrapper<T> responseWrapper = new ResponseWrapper<>(payload);
        responseWrapper.setCode(Constants.SUCCESS_CODE);
        responseWrapper.setMessage(SUCCESS_MESSAGE);
        return responseWrapper;
    }
}
