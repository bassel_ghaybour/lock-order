package com.beeorder.orderslockmc.util;

public abstract class Constants {

    public static final int SUCCESS_CODE = 200;
    public static final int FAILURE_CODE = -1;
    public static final int UNAUTHORIZED_ERROR_CODE = -401;
}
