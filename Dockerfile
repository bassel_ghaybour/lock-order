# Docker file for the Read Service
#
# Version 0.0.1

#jdk image
FROM openjdk:latest

# install

# label for the image
LABEL Description="Eureka Rest Microservice" Version="0.0.1"

# the version of the archive
ARG VERSION=0.0.1

# mount the temp volume
VOLUME /tmp

# Add the service as app.jar
ADD target/orders-lock-mc-${VERSION}-SNAPSHOT.jar docker-rest-microservice.jar

# touch the archive for timestamp
RUN sh -c 'touch /docker-rest-microservice.jar'

# entrypoint to the image on run
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/docker-rest-microservice.jar"]